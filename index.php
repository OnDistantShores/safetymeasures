<?php

require_once("functions.php");

$locationName = null;
$locationData = null;
$questions = array();

// Ensure an address was selected by Google Address Lookup
if (isset($_REQUEST["locationSearchField"]) && isset($_REQUEST["locality"]) && $_REQUEST["locality"]) {
    $locationName = trim($_REQUEST["locationSearchField"]);
    $street_number = trim($_REQUEST["street_number"]);
    $route = trim($_REQUEST["route"]);
    $locality = trim($_REQUEST["locality"]);
    $postal_code = trim($_REQUEST["postal_code"]);

    //$locationData = getDataForLocation($locationName);
    $locationData = getDataForLocation($street_number, $route, $locality, $postal_code);
    $questions = getQuestions();
}
/*if (isset($_REQUEST["locality"])) {
    $locationName = trim($_REQUEST["locality"]);
    $locationData = getDataForLocation($locationName);
}*/

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />

    <title>SafetyMeasures</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/fonts-lora.css" rel="stylesheet" type="text/css">
    <link href="vendor/fonts-montserrat.css" rel="stylesheet" type="text/css">

    <!-- Theme CSS -->
    <link href="css/grayscale.css" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />
    <!--link href="css/progressbar.css" rel="stylesheet" /-->

    <script>
        window.fbAsyncInit = function() {
          FB.init({
            appId      : '472457762917327', // Real app
            //appId      : '472465906249846', // Test app
            xfbml      : true,
            version    : 'v2.3'
          });
        };

        (function(d, s, id){
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement(s); js.id = id;
           js.src = "//connect.facebook.net/en_US/sdk.js";
           fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
    </script>
    <script>
    var placeSearch, autocomplete;
    var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };

    function initAutocomplete() {
      // Create the autocomplete object, restricting the search to geographical
      // location types.
      autocomplete = new google.maps.places.Autocomplete(
          /** @type {!HTMLInputElement} */(document.getElementById('locationSearchField')),
          {types: ['geocode']});

      // When the user selects an address from the dropdown, populate the address
      // fields in the form.
      autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
      // Get the place details from the autocomplete object.
      var place = autocomplete.getPlace();

      for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
      }

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
          var val = place.address_components[i][componentForm[addressType]];
          document.getElementById(addressType).value = val;
        }
      }
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var geolocation = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          var circle = new google.maps.Circle({
            center: geolocation,
            radius: position.coords.accuracy
          });
          autocomplete.setBounds(circle.getBounds());
        });
      }
    }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRvS-PGRG_xL7uCIG224szrKAJoT---zw&libraries=places&callback=initAutocomplete" async defer></script>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <i class="fa fa-life-ring"></i> Safety<span class="light">Measur.es</span>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#search">Find your house</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="brand-heading">Safety<span class="measures">Measur.es</span></h1>
                        <p class="intro-text">How safe are you & your family?
                            <br>How can you protect the people & things you love?</p>
                        <a href="#about" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>About</h2>
                
                <p>SafetyMeasur.es helps you to understand the risk factors in your area, and helps to prepare you for how to make you and your family safer.</p>
                
                <div class="calculation">
                    <div class="calculationStep">
                        <div class="calculationIcon overall">
                            <i class="fa fa-life-ring fa-4x"></i>
                        </div>
                        <div class="title">Your<br />SafetyMeasur.es<br />rating</div>
                    </div>
                    <div class="calculationStep">
                        <div class="calculationMath">=</div>
                    </div>
                    <div class="calculationStep">
                        <div class="calculationIcon location">
                            <i class="fa fa-map fa-4x"></i>
                        </div>
                        <div class="title">Your<br />location<br />rating</div>
                    </div>
                    <div class="calculationStep">
                        <div class="calculationMath">+</div>
                    </div>
                    <div class="calculationStep">
                        <div class="calculationIcon preparedness">
                            <i class="fa fa-umbrella fa-4x"></i>
                        </div>
                        <div class="title">Your<br />preparedness<br />rating</div>
                    </div>
                </div>
                
                <p>This is a <a target='_blank' href='http://2016.govhack.org/'>GovHack 2016 project</a>.
                    It uses IAG flood risk data, CSA Victorian crime data and VicRoads crash data to help you to understand your risks, and
                    raises awareness of support services to help you implement better safety measures for your home, car & family.</p>
                
                <a href="#search" class="btn btn-circle page-scroll">
                    <i class="fa fa-angle-double-down animated"></i>
                </a>
            </div>
        </div>
    </section>

    <section id="search" class="content-section text-center">
        <div class="search-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2>Find your house</h2>
                    <form class="form-inline" action="#initialResults">
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" id="locationSearchField" name="locationSearchField" placeholder="Enter your address..." value="<?php echo $_REQUEST["locationSearchField"]; ?>" />
                        </div>
                        <input type="hidden" id="locality" name="locality" value=""/>
                        <input type="hidden" id="street_number" name="street_number" value=""/>
                        <input type="hidden" id="route" name="route" value=""/>
                        <input type="hidden" id="administrative_area_level_1" name="administrative_area_level_1" value=""/>
                        <input type="hidden" id="country" name="country" value=""/>
                        <input type="hidden" id="postal_code" name="postal_code" value=""/>

                        <button type="submit" class="btn btn-default btn-lg" id="locationSearchAction">Search</button>
                    </form>
                    
                    <?php if (isset($_REQUEST["locationSearchField"]) && $_REQUEST["locationSearchField"] && !$locationName && !$locationData): ?>
                        <div style="color: #F4733D; font-weight: bold; margin-top: 10px; font-size: 18px;">Sorry, your address could not be matched. Please try again, and ensure you select an address match from the drop down box.<br />Also note that only Victorian addresses are supported at this time.</div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <?php if ($locationName && $locationData): ?>
        <section id="initialResults" class="container content-section text-center">
            <div class="row">
                <div class="col-lg-6">
                    <div class="calculationIcon heading location"><i class="fa fa-map fa-2x"></i></div>
                    <h2>
                        Your location rating for <?php echo $street_number . " " . $route . ", " . $locality; ?>
                    </h2>
                    
                    <p class="overallSummary"><strong>Your suburb has an overall SafetyMeasur.es rating of <span class="overallRating"></span></strong>.</p>
                    
                    <p>A higher score = a lower risk. You currently are rated at a <strong><span class="overallRisk"></span></strong>.</p>

                    <!--<p class="floodSummary">Your suburb's <span class="highlight">flood risk rating</span> is a <span class="floodRisk highlight"></span>, giving you a rating of <span class="floodRating highlight"></span>.</p>
                    <p class="crimeSummary">Your area's <span class="highlight">crime rating</span> is a <span class="crimeRisk highlight"></span>, giving you a rating of <span class="crimeRating highlight"></span>.</p>
                    <p class="crashSummary">Your area's <span class="highlight">road safety rating</span> is a <span class="crashRisk highlight"></span>, giving you a rating of <span class="crashRating highlight"></span>.</p>-->

                    <p>But there's much more to your safety than just where you live. There's a number of <strong>safety measures</strong>
                        that you can and should take to make sure people & things you love are kept safe.
                        Find out your real SafetyMeasur.es rating by answering the following questions!</p>
                </div>
                <div class="col-lg-6">
                    <div class="ratings">
                        
                        <div class="ratingSection">
                            <div class="calculationIcon location">
                                <i class="fa fa-map fa-4x"></i>
                            </div>
                            <div class="knobContainer"><input type="text" value="0" class="knob knob-location-flood" /></div>
                            <div class="knobContainer"><input type="text" value="0" class="knob knob-location-crime" /></div>
                            <div class="knobContainer"><input type="text" value="0" class="knob knob-location-crash" /></div>
                        </div>
                        
                        <div class="ratingSection titles">
                            <div class="title floodSummary"><span class="highlight">Flood risk</span></div>
                            <div class="title crimeSummary"><span class="highlight">Crime</span></div>
                            <div class="title crashSummary"><span class="highlight">Road safety</span></div>
                        </div>
                        
                        <div class="ratingSection">
                            <div class="calculationIcon preparedness">
                                <i class="fa fa-umbrella fa-4x"></i>
                            </div>
                            <div class="knobContainer"><input type="text" value="0" class="knob knob-preparedness-flood" /></div>
                            <div class="knobContainer"><input type="text" value="0" class="knob knob-preparedness-crime" /></div>
                            <div class="knobContainer"><input type="text" value="0" class="knob knob-preparedness-crash" /></div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="row preparednessTitle">
                <div class="col-lg-6">
                    <div class="calculationIcon heading preparedness"><i class="fa fa-umbrella fa-2x"></i></div>
                    <h2>
                        Rate your preparedness
                    </h2>
                </div>
            </div>
            <?php
                $i = 0;
                foreach ($questions as $question) {
                    $i++;
                    $questionId = "question" . $i;

                    echo '<div class="row questionContainer" id="' . $questionId . '"><div class="col-lg-6">';
                    echo "<div class='questionTitle'><strong>Question " . $i . "</strong>: " . $question["title"] . "</div>";
                    echo "<div class='answers'></div>";

                    // Is this the last question?
                    if ($i == count($questions)) {
                        echo "<div class='button'><a href='#finalResults' class='page-scroll'>";
                        echo "<button type='button' class='btn btn-default btn-lg' id='lastQuestion-done'>Done</button>";
                        echo "</a></div>";
                    }
                    else {
                        echo "<div class='button'><a href='#question" . ($i + 1) . "' class='nextQuestion page-scroll'>";
                        echo "<button type='button' class='btn btn-default btn-lg' id='" . $questionId . "-next'>Next</button>";
                        echo "</a></div>";
                    }

                    echo '</div></div>';
                }
            ?>
        </section>

        <section id="finalResults" class="container content-section text-center" style="display: none;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="calculationIcon heading overall"><i class="fa fa-life-ring fa-2x"></i></div>
                    <h2>Your SafetyMeasur.es rating for <?php echo $street_number . " " . $route . ", " . $locality; ?></h2>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <p class="overallSummary"><strong>Congratulations! You increased your overall SafetyMeasur.es rating to <span class="overallRating"></span></strong>.</p>
            
                    <div class="finalRatings">
                        <div class="knobContainer">
                            <input type="text" value="0" class="knob knob-final-flood" />
                            <div class="title floodSummary"><span class="highlight">Flood risk</span></div>
                        </div>
                        <div class="knobContainer">
                            <input type="text" value="0" class="knob knob-final-crime" />
                            <div class="title crimeSummary"><span class="highlight">Crime</span></div>
                        </div>
                        <div class="knobContainer">
                            <input type="text" value="0" class="knob knob-final-crash" />
                            <div class="title crashSummary"><span class="highlight">Road safety</span></div>
                        </div>
                    </div>

                    <!--<p class="floodSummary">Your improved <span class="highlight">flood risk rating</span> is <span class="floodRating highlight"></span>.</p>
                    <p class="crimeSummary">Your improved <span class="highlight">crime rating</span> is <span class="crimeRating highlight"></span>.</p>
                    <p class="crashSummary">Your improved <span class="highlight">road safety rating</span> is <span class="crashRating highlight"></span>.</p>-->
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div id='tips'>
                        <p>Here are some ways you could make yourself safer:</p>
                        <ul></ul>
                    </div>
                </div>
                <!--<div class="col-lg-">
                    <div id='moreInfo'>
                        <p>Here is some more info about your area:</p>
                        <ul>
                            <?php
                            
                                /*if (isset($locationData["flood_score"]) && isset($locationData["flood_score_locality"])
                                    && ((round($locationData["flood_score"]) > round($locationData["flood_score_locality"]))
                                        || (round($locationData["flood_score"]) < round($locationData["flood_score_locality"])))) {
                                    echo "<li>Your home's flood rating of " . round($locationData["flood_score"]) . " is "
                                        . (round($locationData["flood_score"]) > round($locationData["flood_score_locality"]) ? "better" : "worse")
                                        . " than the rest of " . $locationData["NAME"]
                                }*/
                            
                            ?>
                            
                            <li>Your home's flood rating of 10/10 is better than the average Yarra Glen home flood rating of 9/10.</li>
                            <li>The majority of car crashes in Yarra Ranges happen at 60hm/h or 80km/h. Last year, 3 of these crashes were fatal.</li>
                            <li>Here's a breakdown of crimes in your area from last year:
                                <div class="graph" style="text-align: center;">
                                    <img src="img/yarraGlenGraph.png" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>-->
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <p>Hopefully you're now more aware of the risks around your home and what you can do to make yourself safer. Please share
                        this site on Facebook so you can help make your friends safer too!<p>

                    <p><button class="btn btn-primary facebook-share">Share my result on Facebook!</button></p>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <hr />
            <p>Created for GovHack 2016 by team "Safety czars"</p>
            <p>For more information about the tech we used and the prizes we are competing for, please visit our <a target='_blank' href='https://2016.hackerspace.govhack.org/content/safetymeasures'>Hackerspace page</a>.</p>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery/jquery.easing.min.js"></script>
    <script src="vendor/jquery/jquery.knob.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/grayscale.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#locationSearchAction").click(function() {

                // TODO Spinner

                /*var location = $("#locationSearchField").val();
                console.log("location = " + location);
                return false;*/
            });

            <?php if ($locationData): ?>
                var initialLocationData = <?php echo json_encode($locationData); ?>;
                var locationData = initialLocationData;
                var answers = {
                    flood: {},
                    crash: {},
                    crime: {},
                };
                var tips = {};

                function getFloodRating() {
                    return parseFloat(locationData.flood_score) + getFloodPreparednessRating();
                }
                function getFloodPreparednessRating() {
                    var floodRating = 0;
                    $.each(answers.flood, function(index, value) {
                        floodRating += parseFloat(value);
                    });
                    return floodRating;
                }
                function getCrimeRating() {
                    return parseFloat(locationData.crime_score) + getCrimePreparednessRating();
                }
                function getCrimePreparednessRating() {
                    var crimeRating = 0;
                    $.each(answers.crime, function(index, value) {
                        crimeRating += parseFloat(value);
                    });
                    return crimeRating;
                }
                function getCrashRating() {
                    return parseFloat(locationData.crash_score) + getCrashPreparednessRating();
                }
                function getCrashPreparednessRating() {
                    var crashRating = 0;
                    $.each(answers.crash, function(index, value) {
                        crashRating += parseFloat(value);
                    });
                    return crashRating;
                }

                function updateRatings() {
                    var floodPreparednessRating = getFloodPreparednessRating();
                    var crimePreparednessRating = getCrimePreparednessRating();
                    var crashPreparednessRating = getCrashPreparednessRating();
                    
                    /*$("div.ratings .floodRating").html(formatRating(floodRating, 10));
                    $("div.ratings .crimeRating").html(formatRating(crimeRating, 10));
                    $("div.ratings .crashRating").html(formatRating(crashRating, 10));
                    $("div.ratings .overallRating").html(formatRating((floodRating + crimeRating + crashRating), 30));
                    
                    $("div.ratings div.lifesaver div.flood").css("height", Math.round((floodRating / 30) * 100) + "%");
                    $("div.ratings div.lifesaver div.crime").css("height", Math.round((crimeRating / 30) * 100) + "%");
                    $("div.ratings div.lifesaver div.crash").css("height", Math.round((crashRating / 30) * 100) + "%");
                    $("div.ratings div.lifesaver div.empty").css("height", Math.round(100 - (((floodRating + crimeRating + crashRating) / 30) * 100)) + "%");*/
                    
                    $("div.ratings .knob-location-flood").val(Math.round((parseFloat(locationData.flood_score) / 10) * 100)).trigger("change");
                    $("div.ratings .knob-location-crime").val(Math.round((parseFloat(locationData.crime_score) / 10) * 100)).trigger("change");
                    $("div.ratings .knob-location-crash").val(Math.round((parseFloat(locationData.crash_score) / 10) * 100)).trigger("change");
                    
                    $("div.ratings .knob-preparedness-flood").val(Math.round((floodPreparednessRating / 10) * 100)).trigger("change");
                    $("div.ratings .knob-preparedness-crime").val(Math.round((crimePreparednessRating / 10) * 100)).trigger("change");
                    $("div.ratings .knob-preparedness-crash").val(Math.round((crashPreparednessRating / 10) * 100)).trigger("change");
                }

                function formatRating(rating, outOf) {
                    return Math.round(rating) + "/" + outOf;
                }

                function getRiskFromScore(rating) {
                    if (rating <= 3) {
                        return "high risk";
                    }
                    else if (rating < 5.5) {
                        return "moderate to high risk";
                    }
                    else if (rating < 8) {
                        return "low to moderate risk";
                    }
                    else {
                        return "low risk";
                    }
                }

                function getOverallRiskFromScore(rating) {
                    if (rating <= 6) {
                        return "high risk";
                    }
                    else if (rating < 12) {
                        return "moderate to high risk";
                    }
                    else if (rating < 18) {
                        return "moderate risk";
                    }
                    else if (rating < 24) {
                        return "low to moderate risk";
                    }
                    else {
                        return "low risk";
                    }
                }
                
                // Set up the pies
                
                var formatReversePercentageOutOf10 = function(value) {
                    if (value == 0) {
                        return "?";
                    }
                    else {
                        return Math.round(value / 100 * 10);
                    }
                };
                var formatReversePercentageOutOf20 = function(value) {
                    return Math.round(value / 100 * 20);
                };
                var sharedConfig = {
                    'format': formatReversePercentageOutOf10,
                    'lineCap': 'round',
                    'width': 120,
                    'height': 120,
                    'readOnly': true,
                    'bgColor': '#bdc3c7',
                    'font': '"Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif',
                    'fontWeight': 700
                };
                $("div.ratings .knob-location-flood").knob($.extend(sharedConfig, {
                    'fgColor': '#3498db',
                }));
                $("div.ratings .knob-location-crime").knob($.extend(sharedConfig, {
                    'fgColor': '#e74c3c'
                }));
                $("div.ratings .knob-location-crash").knob($.extend(sharedConfig, {
                    'fgColor': '#8e44ad'
                }));
                $("div.ratings .knob-preparedness-flood").knob($.extend(sharedConfig, {
                    'fgColor': '#3498db',
                }));
                $("div.ratings .knob-preparedness-crime").knob($.extend(sharedConfig, {
                    'fgColor': '#e74c3c'
                }));
                $("div.ratings .knob-preparedness-crash").knob($.extend(sharedConfig, {
                    'fgColor': '#8e44ad'
                }));
                $("div.finalRatings .knob-final-flood").knob($.extend(sharedConfig, {
                    'format': formatReversePercentageOutOf20,
                    'width': 250,
                    'height': 250,
                    'fgColor': '#3498db',
                }));
                $("div.finalRatings .knob-final-crime").knob($.extend(sharedConfig, {
                    'format': formatReversePercentageOutOf20,
                    'width': 250,
                    'height': 250,
                    'fgColor': '#e74c3c'
                }));
                $("div.finalRatings .knob-final-crash").knob($.extend(sharedConfig, {
                    'format': formatReversePercentageOutOf20,
                    'width': 250,
                    'height': 250,
                    'fgColor': '#8e44ad'
                }));

                // Initial ratings

                updateRatings();

                $("#initialResults .floodSummary .floodRating").html(formatRating(initialLocationData.flood_score, 10));
                $("#initialResults .crimeSummary .crimeRating").html(formatRating(initialLocationData.crime_score, 10));
                $("#initialResults .crashSummary .crashRating").html(formatRating(initialLocationData.crash_score, 10));
                $("#initialResults .overallSummary .overallRating").html(formatRating((parseFloat(initialLocationData.flood_score) + parseFloat(initialLocationData.crime_score) + parseFloat(initialLocationData.crash_score)), 30));

                $("#initialResults .floodSummary .floodRisk").html(getRiskFromScore(initialLocationData.flood_score));
                $("#initialResults .crimeSummary .crimeRisk").html(getRiskFromScore(initialLocationData.crime_score));
                $("#initialResults .crashSummary .crashRisk").html(getRiskFromScore(initialLocationData.crash_score));
                
                $('#initialResults span.overallRisk').html(getOverallRiskFromScore(parseFloat(initialLocationData.flood_score) + parseFloat(initialLocationData.crime_score) + parseFloat(initialLocationData.crash_score)));

                // Build the question answers & handling of responses

                <?php
                    $i = 0;
                    foreach ($questions as $question) {
                        $i++;
                        $questionId = "question" . $i;

                        echo "answers." . $question["group"] . "['" . $questionId . "'] = 0;";

                        foreach ($question["answers"] as $answer) {

                            if ($question["type"] == "radio") {
                            ?>
                                tips["<?php echo $questionId;?>"] = null;
                                $("#<?php echo $questionId;?> div.answers").append(
                                    $("<div />")
                                        .addClass("answer")
                                        .addClass("radio")
                                        .append(
                                            $("<label />")
                                                .append(
                                                    $("<input />")
                                                        .attr("type", "radio")
                                                        .attr("name", "<?php echo $questionId;?>")
                                                        .attr("value", "<?php echo $answer["score"]; ?>")
                                                        .change(function() {
                                                            answers.<?php echo $question["group"]; ?>["<?php echo $questionId;?>"] = $("input:radio[name ='<?php echo $questionId;?>']:checked").val();

                                                            tips["<?php echo $questionId;?>"] = <?php echo (isset($answer["positiveTip"]) ? "\"" . $answer["positiveTip"] . "\"" : "null"); ?>;

                                                            updateRatings();
                                                        })
                                                )
                                                .append(
                                                    $("<span />").html("<?php echo $answer["title"]; ?>")
                                                )
                                        )
                                );
                            <?php
                            }
                            else {
                            ?>
                                tips["<?php echo $questionId . "." . $answer["value"];?>"] = <?php echo (isset($answer["negativeTip"]) ? "\"" . $answer["negativeTip"] . "\"" : "null"); ?>;
                                $("#<?php echo $questionId;?> div.answers").append(
                                    $("<div />")
                                        .addClass("answer")
                                        .addClass("checkbox")
                                        .append(
                                            $("<label />")
                                                .append(
                                                    $("<input />")
                                                        .attr("type", "checkbox")
                                                        .change(function() {
                                                            if ($(this).is(":checked")) {
                                                                answers.<?php echo $question["group"]; ?>["<?php echo $questionId;?>"] += <?php echo $answer["score"]; ?>;

                                                                tips["<?php echo $questionId . "." . $answer["value"];?>"] = null;
                                                            }
                                                            else {
                                                                answers.<?php echo $question["group"]; ?>["<?php echo $questionId;?>"] -= <?php echo $answer["score"]; ?>;

                                                                tips["<?php echo $questionId . "." . $answer["value"];?>"] = <?php echo (isset($answer["negativeTip"]) ? "\"" . $answer["negativeTip"] . "\"" : "null"); ?>;
                                                            }

                                                            updateRatings();
                                                        })
                                                )
                                                .append(
                                                    $("<span />").html("<?php echo $answer["title"]; ?>")
                                                )
                                        )
                                );
                            <?php
                            }
                        }
                    }
                ?>
                
                // Don't allow clicking the questions you haven't got to yet
                
                $(".questionContainer input, .questionContainer button").attr("disabled", "disabled");
                $("#question1 input, #question1 button").removeAttr("disabled");

                // Handle revealing questions as the previous "next" is clicked

                $(".questionContainer .nextQuestion").click(function() {
                    $($(this).attr("href")).css("opacity", 1);
                    $($(this).attr("href") + " input, " + $(this).attr("href") + " button").removeAttr("disabled");
                });
                $("#question1").css("opacity", 1); // Show the first one to start with

                // Don't allow clicking the questions you haven't got to yet

                $(".questionContainer input, .questionContainer button").attr("disabled", "disabled");
                $("#question1 input, #question1 button").removeAttr("disabled");

                // Handle revealing questions as the previous "next" is clicked

                $(".questionContainer .nextQuestion").click(function() {
                    $($(this).attr("href")).css("opacity", 1);
                    $($(this).attr("href") + " input, " + $(this).attr("href") + " button").removeAttr("disabled");
                });
                $("#question1").css("opacity", 1); // Show the first one to start with

                // Allow the affixing of the scoring
    
                var initialResultsHeaderOffset = $("#initialResults h2").offset();
                $('div.ratings').affix({
                    offset: {
                      top: initialResultsHeaderOffset.top - 80,
                      bottom: 200
                    }
                });

                // Build the display of the final report

                $("#lastQuestion-done").click(function() {
                    $("#finalResults").show();

                    var floodRating = getFloodRating();
                    $("#finalResults .floodSummary .floodRating").html(formatRating(floodRating, 20));
                    $(".knob-final-flood").val(Math.round((floodRating / 20) * 100)).trigger("change");

                    var crimeRating = getCrimeRating();
                    $("#finalResults .crimeSummary .crimeRating").html(formatRating(crimeRating, 20));
                    $(".knob-final-crime").val(Math.round((crimeRating / 20) * 100)).trigger("change");

                    var crashRating = getCrashRating();
                    $("#finalResults .crashSummary .crashRating").html(formatRating(crashRating, 20));
                    $(".knob-final-crash").val(Math.round((crashRating / 20) * 100)).trigger("change");

                    $("#finalResults .overallSummary .overallRating").html(formatRating((floodRating + crimeRating + crashRating), 60));

                    var tipsAdded = 0;
                    $.each(tips, function(index, value) {
                        if (value) {
                            tipsAdded++;

                            $("#tips ul").append(
                                $("<li>").html(value)
                            );
                        }
                    });

                    if (tipsAdded == 0) {
                        $("#tips").hide();
                    }
                    
                    // Re-affix now that this section is built
                    lastButtonOffset = $("#lastQuestion-done").offset();
                    $('div.ratings').data('bs.affix').options.offset.bottom = ($(document).height() - lastButtonOffset.top - 100);
                });
                
                // Facebook share

                $(".facebook-share").click(function() {
                    var floodRating = getFloodRating();
                    var crimeRating = getCrimeRating();
                    var crashRating = getCrashRating();
                    
                    FB.ui({
                        method: 'feed',
                        link: 'http://safetymeasures.invitationstation.org/',
                        caption: 'I scored ' + Math.round(floodRating + crimeRating + crashRating) + ' out of <?php echo 30; ?> on SafetyMeasur.es, my personal safety assessment. See if you can do better!',
                    }, function(response){});
                });

            <?php endif; ?>
        });
    </script>
</body>

</html>
