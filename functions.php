<?php

define("DB_HOST", "localhost");
define("DB_NAME", "safetymeasures");
define("DB_USERNAME", "root");
define("DB_PASSWORD", "root");

function dbConnect() {
    mysql_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME) or die("Failed to connect to MySQL: " . mysql_error());
    mysql_select_db(DB_NAME);
}

function getDataForLocation($street_number, $route, $locality, $postal_code) {

    $file = fopen("safetyMeasuresDB.csv","r");

    while(!feof($file)){
      $dataRow = fgetcsv($file);

      $locationData = array(
          "LOC_PID" => $dataRow[0] ,
          "NAME" => $dataRow[1] ,
          "LGA_PID" => $dataRow[2] ,
          "LGA_NAME" => $dataRow[3] ,
          "Flood_COUNT_LOW_AAD" => $dataRow[4] ,
          "Flood_COUNT_MEDIUM_AAD" => $dataRow[5] ,
          "Flood_COUNT_HIGH_AAD" => $dataRow[6] ,
          "Flood_COUNT_LOW_FREQUENCY" => $dataRow[7] ,
          "Flood_COUNT_MEDIUM_FREQUENCY" => $dataRow[8] ,
          "Flood_COUNT_HIGH_FREQUENCY" => $dataRow[9] ,
          "Flood_RANK_AAD" => $dataRow[10] ,
          "flood_score" => $dataRow[11] ,
          "flood_score_locality" => $dataRow[11] ,
          "Crime_LGA_ERP" => $dataRow[12] ,
          "Crime_A_Crimes_against_the_person" => $dataRow[13] ,
          "Crime_B_Property_and_deception_offences" => $dataRow[14] ,
          "Crime_C_Drug_offences" => $dataRow[15] ,
          "Crime_D_Public_order_and_security_offences" => $dataRow[16] ,
          "Crime_E Justice_procedures_offences" => $dataRow[17] ,
          "Crime_F_Other_offences" => $dataRow[18] ,
          "Crime_A10_Homicide_and_related_offences" => $dataRow[19] ,
          "Crime_A20_Assault_and_related_offences" => $dataRow[20] ,
          "Crime_A30_Sexual_offences" => $dataRow[21] ,
          "Crime_A40_Abduction_and_related_offences" => $dataRow[22] ,
          "Crime_A50 Robbery" => $dataRow[23] ,
          "Crime_A60_Blackmail_and_extortion" => $dataRow[24] ,
          "Crime_A70_Stalking_harassment_and_threatening_behaviour" => $dataRow[25] ,
          "Crime_A80_Dangerous_and_negligent_acts_endangering_people" => $dataRow[26] ,
          "Crime_B10_Arson" => $dataRow[27] ,
          "Crime_B20_Property_damage" => $dataRow[28] ,
          "Crime_B30_Burglary_Break_and_enter" => $dataRow[29] ,
          "Crime_B40_Theft" => $dataRow[30] ,
          "Crime_B50_Deception" => $dataRow[31] ,
          "Crime_B60_Bribery" => $dataRow[32] ,
          "Crime_C10_Drug_dealing_and_trafficking" => $dataRow[33] ,
          "Crime_C20_Cultivate_or_manufacture_drugs" => $dataRow[34] ,
          "Crime_C30_Drug_use_and_possession" => $dataRow[35] ,
          "Crime_C90_Other_drug_offences" => $dataRow[36] ,
          "Crime_D10_Weapons_and_explosives_offences" => $dataRow[37] ,
          "Crime_D20_Disorderly_and_offensive_conduct" => $dataRow[38] ,
          "Crime_D30_Public_nuisance_offences" => $dataRow[39] ,
          "Crime_D40_Public_security_offences" => $dataRow[40] ,
          "Crime_E10_Justice_procedures" => $dataRow[41] ,
          "Crime_E20_Breaches_of_orders" => $dataRow[42] ,
          "Crime_F10_Regulatory_driving_offences" => $dataRow[43] ,
          "Crime_F20_Transport_regulation_offences" => $dataRow[44] ,
          "Crime_F30_Other_government_regulatory_offences" => $dataRow[45] ,
          "Crime_F90_Miscellaneous_offences" => $dataRow[46] ,
          "Crime_Total" => $dataRow[47] ,
          "crime_score" => $dataRow[48] ,
          "Crash_TYPE_Fatal_accident" => $dataRow[49] ,
          "Crash_TYPE_Other_injury_accident" => $dataRow[50] ,
          "Crash_TYPE_Serious_injury_accident" => $dataRow[51] ,
          "Crash_Alcohol_Related_NO" => $dataRow[52] ,
          "Crash_Allcohol_Related_YES" => $dataRow[53] ,
          "Crash_Speed_Zone_100" => $dataRow[54] ,
          "Crash_Speed_Zone_110" => $dataRow[55] ,
          "Crash_Speed_Zone_30" => $dataRow[56] ,
          "Crash_Speed_Zone_40" => $dataRow[57] ,
          "Crash_Speed_Zone_50" => $dataRow[58] ,
          "Crash_Speed_Zone_60" => $dataRow[59] ,
          "Crash_Speed_Zone_70" => $dataRow[60] ,
          "Crash_Speed_Zone_80" => $dataRow[61] ,
          "Crash_Speed_Zone_90" => $dataRow[62] ,
          "Crash_Speed_Zone_Camping_grounds_or_off_road" => $dataRow[63] ,
          "Crash_Speed_Zone_Not_known" => $dataRow[64] ,
          "Crash_Speed_Zone_Other_speed_limit" => $dataRow[65] ,
          "Crash_Total" => $dataRow[66] ,
          "crash_score" => $dataRow[67]
      );

      if($locationData["LGA_NAME"] == strtoupper($locality) || $locationData["NAME"] == strtoupper($locality)){
        break;
      }
      else {
        unset($locationData);
      }
    }

    //Getting average_annual_damage and flood_frequency indications of the actual address from the IAG Flood data API
    $url = 'http://flood-risk-api.app.skyops.io/address-flood-risk/search';
    $ch = curl_init($url);

    $jsonData = array(
      'full_address' => $street_number . " " . $route,
      'locality' => $locality,
      'postcode' => $postal_code,
    );

    $jsonDataEncoded = json_encode($jsonData);
    $headers = array(
    'Content-Type: application/json',
    'Accept: application/json',
    'x-iag-api-key: iag-gov-hack-api');

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    $resultJson = curl_exec($ch);

    $result = json_decode($resultJson, true);

    if(isset($result[0]['average_annual_damage']) && isset($result[0]['flood_frequency'])){
      $average_annual_damage = $result[0]['average_annual_damage'];
      $flood_frequency = $result[0]['flood_frequency'];

      $floodRisk = $average_annual_damage . $flood_frequency;
      
      switch($floodRisk) {
        case 'LL' :
            $locationData["flood_score"] = 10;
            break;
        case 'LM' :
            $locationData["flood_score"] = 8;
            break;
        case 'LH' :
            $locationData["flood_score"] = 8;
            break;
        case 'ML' :
            $locationData["flood_score"] = 6;
            break;
        case 'MM' :
            $locationData["flood_score"] = 6;
            break;
        case 'MH' :
            $locationData["flood_score"] = 4;
            break;
        case 'HL' :
            $locationData["flood_score"] = 4;
            break;
        case 'HM' :
            $locationData["flood_score"] = 2;
            break;
        case 'HH' :
            $locationData["flood_score"] = 1;
            break;
      }

    }
    return $locationData;
}

function getQuestions() {
    return array(
        array(
            "title" => "Do you have insurance, should your house get flooded?",
            "group" => "flood",
            "type" => "radio",
            "answers" => array(
                array(
                    "title" => "I don't have insurance",
                    "value" => "donthave",
                    "score" => 0.1,
                    "positiveTip" => "Having the right home insurance cover is critically important! This will help keep you and your family stress free both before and after an event.",
                ),
                array(
                    "title" => "I have insurance, but I'm not sure if I have flood cover",
                    "value" => "floodnotsure",
                    "score" => 1,
                    "positiveTip" => "Use an insurance calculator such as <a target='_blank' href='http://www.nrma.com.au/Insurance‎'>www.nrma.com.au/Insurance‎</a> to ensure your coverage is correct.",
                ),
                array(
                    "title" => "I have insurance with flood cover, but I wouldn't know how to get all my stuff replaced",
                    "value" => "wouldntknowstuff",
                    "score" => 1.5,
                    "positiveTip" => "You can find a good guide on how to catalogue your possessions on <a target='_blank' href='https://www.budgetdirect.com.au/blog/take-proper-inventory-for-your-home-and-contents-insurance-claims.html'>BudgetDirect's site</a>.",
                ),
                array(
                    "title" => "I have insurance with flood cover and I've catalogued & photographed all my stuff so I can ensure it all gets replaced",
                    "value" => "allgood",
                    "score" => 2,
                ),
            ),
        ),
        array(
            "title" => "Do you have these elements of a home emergency plan in place?",
            "group" => "flood",
            "type" => "checkboxes",
            "answers" => array(
                array(
                    "title" => "Identified where you would go in the event of an emergency? e.g. Evacuation centres set-up by SES and local councils etc.",
                    "value" => "evacuation",
                    "score" => 2,
                    "negativeTip" => "Make sure to identify a safe, easy to reach evacuation point for you and your family. Refer to <a target='_blank' href='http://emergency.vic.gov.au/prepare'>emergency.vic.gov.au/prepare</a> for information on being prepared.",
                ),
                array(
                    "title" => "Identified who to call and have emergency contact numbers handy",
                    "value" => "contactnumbers",
                    "score" => 1,
                    "negativeTip" => "You should call the SES on 1300 VICSES (1300 842 737) in case of an emergency, or the national emergency helpline on 000.",
                ),
                array(
                    "title" => "Identified the safest route to travel in the event that you might need to evacuate & identify the height at which your evacuation route may be cut",
                    "value" => "safestroute",
                    "score" => 1,
                    "negativeTip" => "Planning a safe evacuation route in advance will go a long way in getting you to safety. Plan a safe route in advance and keep a printed copy in your emergency toolkit.",
                ),
                array(
                    "title" => "Identified which local radio stations will have the most helpful information",
                    "value" => "localradio",
                    "score" => 0.5,
                    "negativeTip" => "Make sure to know the emergency radio frequency in your area in advance and tune in for the latest updates as you evacuate. Find out about your local <a target='_blank' href='http://fire-com-live-wp.s3.amazonaws.com/wp-content/uploads/EM-Broadcasters.pdf'>Emergency Broadcasters here</a>.",
                ),
            ),
        ),
        array(
            "title" => "Do you have the free emergency app installed on your phone?",
            "group" => "flood",
            "type" => "radio",
            "answers" => array(
                array(
                    "title" => "Yes",
                    "value" => "yesapp",
                    "score" => 3.5,
                ),
                array(
                    "title" => "No",
                    "value" => "noapp",
                    "score" => 0,
                    "positiveTip" => "The free <a target='_blank' href='http://www.triplezero.gov.au/Pages/EmergencySmartphoneApp.aspx'> Emergency App </a> is a Triple Zero (000) smartphone app that provides key numbers for the emergency services and displays the GPS coordinates of your location so you can read it out to the emergency operator.",
                ),
            ),
        ),
        array(
            "title" => "Are you aware of the crime hotspots around your usual destinations? (e.g. Home, work, local parks, recreation centres, etc)",
            "group" => "crime",
            "type" => "radio",
            "answers" => array(
                array(
                    "title" => "I've never thought about it - it will never happen to me in my area.",
                    "value" => "notthoughtaboutcrime",
                    "score" => 0.1,
                    "positiveTip" => "Being aware is the first step to avoid being a victim regardless of where you are. Have a quick look at the <a target='_blank' href='http://www.police.vic.gov.au/content.asp?Document_ID=782'>Crime Statistics Agency statistics</a> to see which areas to avoid / be more vigilant in.",
                ),
                array(
                    "title" => "I'm aware of the hotspots, but I can defend myself so I don't need to worry",
                    "value" => "awarebutnottoworry",
                    "score" => 2,
                    "positiveTip" => "Avoiding being a conflict always the best outcome, even if you can defend yourself, consider avoiding the known crime hotspots. <a target='_blank' href='http://www.police.vic.gov.au/content.asp?Document_ID=782'>Crime Statistics Agency statistics</a>.",
                ),
                array(
                    "title" => "I'm aware of the crime hotspots and keep an eye out to avoid them.",
                    "value" => "awareandvoid",
                    "score" => 4,
                ),
            ),
        ),
        array(
            "title" => "When did you do the last safety review of your house, including your door locks & windows?",
            "group" => "crime",
            "type" => "radio",
            "answers" => array(
                array(
                    "title" => "Never, I've never gotten around to it.",
                    "value" => "nosafetyreview",
                    "score" => 0,
                    "positiveTip" => "Reviewing the safety of your house regularly will not only keep you stress-free but also go a long way in protecting your hard earned assets. Follow the <a target='_blank' href='http://www.australianpolice.com.au/crime-prevention-tips-home-security/'>Home Security Tips from Victorian Police</a>.",
                ),
                array(
                    "title" => "More than 6 months ago",
                    "value" => "safestroute",
                    "score" => 2,
                    "positiveTip" => "Regular safety reviews are very important to ensure your safety measures remain effective againt new crime trends. Follow the <a target='_blank' href='http://www.australianpolice.com.au/crime-prevention-tips-home-security/'>Home Security Tips from Victorian Police</a> to review the safety of your house.",
                ),
                array(
                    "title" => "In the last 6 months",
                    "value" => "lastmonthsafetyreview",
                    "score" => 4,
                ),
            ),
        ),
        array(
            "title" => "Do you have home and contents insurance with adequate coverage?",
            "group" => "crime",
            "type" => "checkboxes",
            "answers" => array(
                array(
                    "title" => "I have my specific valuables covered (e.g. specific jewelry, collections, etc)",
                    "value" => "onlyvaluablescovered",
                    "score" => 2,
                    "negativeTip" => "Make sure to review your insurance coverage regularly. As we buy new things, the insurance coverage can very quickly get out of date. Review your insurance with <a target='_blank' href='http://www.canstar.com.au/home-insurance/'>Canstar Home Insurance</a>.",
                ),
                array(
                    "title" => "I'm covered for my everyday items (e.g. clothes, etc)",
                    "value" => "everydayitemscovered",
                    "score" => 2,
                    "negativeTip" => "Having adequte coverage is very important as sudden costs to replace everyday items can have a profound unwanted affect on your lifestyle. Review your insurance with <a target='_blank' href='http://www.canstar.com.au/home-insurance/'>Canstar Home Insurance</a>.",
                ),
            ),
        ),
        array(
            "title" => "Have you done a vehicle safety check to ensure that your car is adequately safe? Is your car regularly serviced?",
            "group" => "crash",
            "type" => "radio",
            "answers" => array(
                array(
                    "title" => "I'll never be in a crash.",
                    "value" => "willneverbeinacrash",
                    "score" => 0.1,
                    "positiveTip" => "Even the safest drivers have a moderate crash risk due to factors outside their control. Check the safety of your car at <a target='_blank' href='http://www.howsafeisyourcar.com.au/how-safe-is-my-car/'>howsafeisyourcar.com.au/</a>.",
                ),
                array(
                    "title" => "I'm aware of my vehicle's safety features but haven't had a car service for over 1 year",
                    "value" => "awarebutnotserviced",
                    "score" => 2,
                    "positiveTip" => "Servicing your car regularly keeps your vehicles safety features in perfect working condition. Make sure to book in your car for a service as soon as possible to keep you and your loved ones safe.",
                ),
                array(
                    "title" => "I'm aware of my vehicles safety features and have my car regularly serviced.",
                    "value" => "awareandvoid",
                    "score" => 4,
                ),
            ),
        ),
        array(
            "title" => "Do you familiarise yourself with the crash blackspots in your regular travel routes and when travelling in unfamiliar territory?",
            "group" => "crash",
            "type" => "radio",
            "answers" => array(
                array(
                    "title" => "It's ok, I'm a safe driver so I don't need to know about the blackspots.",
                    "value" => "safedrivernoblackspots",
                    "score" => 0,
                    "positiveTip" => "Accidents are not always the driver's fault. Unfamiliar territory can dramatically increase chances of a crash. Familiarise yourself with the <a target='_blank' href='http://investment.infrastructure.gov.au/funding/blackspots/'>Australian government's blackspot program</a>.",
                ),
                array(
                    "title" => "I know about the blackspots in my regular driving routes.",
                    "value" => "knowregularroutes",
                    "score" => 2,
                    "positiveTip" => "Unfamiliar territory can dramatically increase chances of a crash. Be sure to familiarise yourself with the <a target='_blank' href='http://investment.infrastructure.gov.au/funding/blackspots/'>Australian government's blackspots</a> when travelling in unfamiliar areas."
                ),
                array(
                    "title" => "I regularly check for blackspots when travelling.",
                    "value" => "regularlycheckblackspots",
                    "score" => 4,
                ),
            ),
        ),
        array(
            "title" => "Do you PokemonGo and drive? Or text/Snapchat and drive?",
            "group" => "crash",
            "type" => "radio",
            "answers" => array(
                array(
                    "title" => "Yes",
                    "value" => "yespokemongo",
                    "score" => 0,
                    "positiveTip" => "It goes without saying that PokemonGo and driving is dangerous business! Stop it!",
                ),
                array(
                    "title" => "No",
                    "value" => "nopokemongo",
                    "score" => 2,
                ),
            ),
        ),
    );
}
